﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private string dinosaur;
        private int age;


        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            if (age < 0)
            {
                this.age = -age;
            }
            else
                this.age = age;
            dinosaur = name;
        }
        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }
        public string roar()
        {
            return "Grrr";
        }
        public string hug(Dinosaur a)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}.", this.name, a.name);
        }

        public string getName() //recupere le nom qui est dans name pour affiche le nom du dino
        {
            return this.name;
        }
        public string getSpecie()
        {
            return this.specie;
        }
        public int getAge()
        {
            return this.age;
        }

        public void setName(string name) //défini le nom dans name pour permet de changer le nom du dino
        {
            this.name = name;
        }
        public void setSpecie(string specie)
        {
            this.specie = specie;
        }
        public void setAge(int age)
        {
            if (age < 0)
            {
                this.age = -age;
            }
            else
                this.age = age;
        }
        
    }
}
