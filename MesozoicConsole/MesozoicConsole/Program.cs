﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace MesozoicConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Dinosaur nessie, louis;
            string name;
            string specie;
            string verifAge;
            int age;

            nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            louis = new Dinosaur("louis", "Stegausaurus", 12);
            Dinosaur dino1 = new Dinosaur(null, null, 0);

            Console.WriteLine("Quel est le nom de votre dino : ");
            name = Console.ReadLine();
            Console.WriteLine("Quel est son espece : ");
            specie = Console.ReadLine();
            do
            {
                Console.Write("Quel est son age : ");

                do
                {
                    verifAge = Console.ReadLine();
                } while (!int.TryParse(verifAge, out age));
                if (age < 0) Console.WriteLine("{0} is invalid", age);
            } while (age < 0);
            dino1.setAge(age);
            dino1.setName(name);
            dino1.setSpecie(specie);

            /*Console.WriteLine(dino.setName(name));
            Console.WriteLine(dino.setSpecie(specie));
            Console.WriteLine(dino.setAge(age));
            */
            Console.WriteLine(dino1.sayHello());
            Console.WriteLine(nessie.sayHello());
            Console.WriteLine(louis.sayHello());
            Console.WriteLine(nessie.hug(louis));



            Console.ReadKey();
        }
    }
}
