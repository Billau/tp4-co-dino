﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class DinosaurTest
    {
       
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }
        /*[TestMethod]
        public void TestDinosaurhug()
        {
            Dinosaur louis = new Dinosaur("Nessie", "Stegausaurus", 12);
            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug());
        }*/
        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestDinosaursetName()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosorus", 11);
            Assert.AreEqual("Louis", louis.getName());
            louis.setName("Victor");
            Assert.AreEqual("Victor", louis.getAge());
        }
        [TestMethod]
        public void TestDinosaursetAge()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosorus", 11);
            Assert.AreEqual(11, louis.getAge());
            louis.setAge(12);
            Assert.AreEqual(12, louis.getAge());
        }
        [TestMethod]
        public void TestDinosaursetSpecie()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosorus", 11);
            Assert.AreEqual("Stegosorus", louis.getSpecie());
            louis.setSpecie("cheval");
            Assert.AreEqual("cheval", louis.getSpecie());
        }
    }
}
